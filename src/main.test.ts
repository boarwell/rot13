import { rot13 } from './main.js';

type TestCase = {
  input: string;
  expected: string;
};

function test() {
  const testCases: TestCase[] = [
    {
      input: 'a',
      expected: 'n'
    },
    {
      input: 'A',
      expected: 'N'
    },
    {
      input: 'And',
      expected: 'Naq'
    },
    {
      input: '日本語 And English',
      expected: '日本語 Naq Ratyvfu'
    }
  ];

  console.log(
    JSON.stringify(
      testCases
        .filter(({ input, expected }) => rot13(input) !== expected)
        .map(({ input, expected }) => ({ input, expected, got: rot13(input) }))
    )
  );
}

test();
