function isSmall(c: string): boolean {
  const codePoint = c.codePointAt(0)!;
  return 'a'.codePointAt(0)! <= codePoint && codePoint <= 'z'.codePointAt(0)!;
}

function isCapital(c: string): boolean {
  const codePoint = c.codePointAt(0)!;
  return 'A'.codePointAt(0)! <= codePoint && codePoint <= 'Z'.codePointAt(0)!;
}

/**
 * アルファベットかどうか
 */
function isAlphabet(c: string): boolean {
  return isCapital(c) || isSmall(c);
}

function core(char: string, base: string): string {
  const baseCodePoint = base.codePointAt(0)!;

  return String.fromCodePoint(
    ((char.codePointAt(0)! - baseCodePoint + 13) % 26) + baseCodePoint
  );
}

/**
 * 1文字を受け取ってrot(13)を返す
 * アルファベット以外は入力値をそのまま返す
 *
 * @example rot('a') -> 'n'
 * @example rot('A') -> 'N'
 * @example rot('n') -> 'a'
 */
function rot13Char(c: string): string {
  return core(c, isCapital(c) ? 'A' : 'a');
}

export function rot13(s: string): string {
  return [...s].map(c => (isAlphabet(c) ? rot13Char(c) : c)).join('');
}
